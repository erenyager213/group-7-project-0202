
//Query DOM
const talkwords = document.getElementById('talkwords');
const talksub = document.getElementById('talksub');
const publicWord = document.getElementById('public-word');
const clientsTotal = document.getElementById('client-total');
const clientsList = document.getElementById('client-list');
let chatBotMode = false;
const teckyBot = document.getElementById('tecky-bot');
const everyone = document.getElementById('everyone');
let senderName;
let delaytTime = 1500;
//var socket = io.connect('localhost:8080');

window.onload = () => {
    //window.location.host
    const socket = io.connect('localhost:8080');

    socket.on('userName', function (data) {
        // console.log(data);
        userName = data;
    })

    socket.on('clients-total', function (data) {
        // console.log(data);
        clientsTotal.innerText = `Total Clients: ${data}`
    })

    socket.on('clients-list', function (arrayUserName) {
        clientsList.innerHTML = "<div></div>";
        for (let i = 0; i < arrayUserName.length; i++) {
            const div = document.createElement('div');
            div.innerHTML = `<span>${arrayUserName[i]}<span>`
            clientsList.appendChild(div);
            clientsList.scrollTop = clientsList.scrollHeight;
        }
    })


    teckyBot.addEventListener('click', function () {
        if (!chatBotMode) {
            chatBotMode = true;
            teckyBot.innerHTML = "Talk To All";
            const div = document.createElement('div');
            div.classList.add('msg');
            div.classList.add('left-msg');
            div.innerHTML = `<div
            class="msg-img"
            style="background-image: url(asset/icons8-bot.png)"
           ></div>
     
           <div class="msg-bubble">
             <div class="msg-info">
               <div class="msg-info-name">TeckyBot</div>
               <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
             </div>
     
             <div class="msg-text">
               Hello~
             </div>
           </div>`;
            publicWord.appendChild(div);
            publicWord.scrollTop = publicWord.scrollHeight;

        } else {
            chatBotMode = false;
            teckyBot.innerHTML = "TeckyBot";
            const div = document.createElement('div');
            div.classList.add('msg');
            div.classList.add('left-msg');
            div.innerHTML = `<div
            class="msg-img"
            style="background-image: url(asset/icons8-bot.png)"
           ></div>
     
           <div class="msg-bubble">
             <div class="msg-info">
               <div class="msg-info-name">TeckyBot</div>
               <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
             </div>
     
             <div class="msg-text">
               無咩我出番去先了
             </div>
           </div>`;
            publicWord.appendChild(div);
            publicWord.scrollTop = publicWord.scrollHeight;
        }
        return;
    });
    //Enter to Chat Bot mode     
    // teckyBot.addEventListener('click', function () {
    //     chatBotMode = true;
    //     const div = document.createElement('div');
    //     div.classList.add('msg');
    //     div.classList.add('left-msg');
    //     div.innerHTML = `<div
    //         class="msg-img"
    //         style="background-image: url(asset/icons8-bot.png)"
    //        ></div>

    //        <div class="msg-bubble">
    //          <div class="msg-info">
    //            <div class="msg-info-name">TeckyBot</div>
    //            <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
    //          </div>

    //          <div class="msg-text">
    //            Hello~
    //          </div>
    //        </div>`;
    //     publicWord.appendChild(div);
    //     publicWord.scrollTop = publicWord.scrollHeight;
    // });

    //Leave the Chat Bot mode
    // everyone.addEventListener('click', function () {
    //     chatBotMode = false;
    //     const div = document.createElement('div');
    //     div.classList.add('msg');
    //     div.classList.add('left-msg');
    //     div.innerHTML = `<div
    //         class="msg-img"
    //         style="background-image: url(asset/icons8-bot.png)"
    //        ></div>

    //        <div class="msg-bubble">
    //          <div class="msg-info">
    //            <div class="msg-info-name">TeckyBot</div>
    //            <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
    //          </div>

    //          <div class="msg-text">
    //            無咩我出番去先了
    //          </div>
    //        </div>`;
    //     publicWord.appendChild(div);
    //     publicWord.scrollTop = publicWord.scrollHeight;
    // })


    /*
      
      talkwords.addEventListener('focus',function(data){
          socket.emit('feedback',{
              feedback:`is tying a message...`,
          })
      })
      talkwords.addEventListener('keypress',function(data){
          socket.emit('feedback',{
              feedback:`is tying a message...`,
          })
      })
      talkwords.addEventListener('focus',function(data){
          socket.emit('feedback',{
              feedback:'',
          })
      })
  
      socket.on('feedback',function(data){
          const element = `
          <li class="message-feedback">
                        <p class= "feedback" id="feedback">${data.typingPerson + " " }${data.feedback} </p>
                    </li>
                    `
       publicWord.innerHTML += element;
       element = '';
      })
  
      */


    //Emit event
    talksub.addEventListener('click', function (event) {
        if (chatBotMode == false) {
            event.preventDefault();
            //the data come from the typeing area
            const data = {
                talkwords: talkwords.value,
                time: new Date().toLocaleTimeString()
            };

            //send the data to the server with type 'chat'
            socket.emit('chat', data);


            //display the typing area data to the sender public chating section
            const div = document.createElement('div');
            div.classList.add('msg');
            div.classList.add('right-msg');
            div.innerHTML = `<div
            class="msg-img"
            style="background-image: url(asset/icons8-hacker.png)"
           ></div>
     
           <div class="msg-bubble">
             <div class="msg-info">
               <div class="msg-info-name">me</div>
               <div class="msg-info-time">${data.time}</div>
             </div>
     
             <div class="msg-text">
               ${data.talkwords}
             </div>
           </div>`;

            publicWord.appendChild(div);
            publicWord.scrollTop = publicWord.scrollHeight;

            talkwords.value = '';
            talkwords.focus();

        };


        if (chatBotMode == true) {
            event.preventDefault();

            //the data come from the typeing area
            const sendToBotData = {
                talkwords: talkwords.value
            };

            //send the data to the server with type 'chat'
            socket.emit('bot-chat', sendToBotData);


            //display the typing area data to the sender public chating section
            const div = document.createElement('div');
            div.classList.add('msg');
            div.classList.add('right-msg');
            div.innerHTML = `<div
            class="msg-img"
            style="background-image: url(asset/icons8-hacker.png)"
           ></div>
     
           <div class="msg-bubble">
             <div class="msg-info">
               <div class="msg-info-name">me</div>
               <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
             </div>
     
             <div class="msg-text">
               ${sendToBotData.talkwords}
             </div>
           </div>`;

            publicWord.appendChild(div);
            publicWord.scrollTop = publicWord.scrollHeight;

            talkwords.value = '';
            talkwords.focus();


        }



    });
    //Listen for events that the data sent from other user
    socket.on('chat', function (data) {
        console.log(`other user just sent the data: ${data}`)

        //display other user data with this css
        const div = document.createElement('div');
        div.classList.add('msg');
        div.classList.add('left-msg');
        div.innerHTML = `<div
    class="msg-img"
    style="background-image: url(asset/icons8-hacker.png)"
   ></div>

   <div class="msg-bubble">
     <div class="msg-info">
       <div class="msg-info-name">${data.sendPerson}</div>
       <div class="msg-info-time">${data.time}</div>
     </div>

     <div class="msg-text">
       ${data.talkwords} 
     </div>
   </div>`;

        publicWord.appendChild(div);
        publicWord.scrollTop = publicWord.scrollHeight;
    });


    //Listen for events that the data sent from other user
    socket.on('bot-chat', function (sendToBotData) {
        console.log(`other user just sent the data: ${sendToBotData.talkwords}`)


        //display other user data with this css
        const div = document.createElement('div');
        div.classList.add('msg');
        div.classList.add('left-msg');
        div.innerHTML = `<div
    class="msg-img"
    style="background-image: url(asset/icons8-hacker.png)"
   ></div>

   <div class="msg-bubble">
     <div class="msg-info">
       <div class="msg-info-name">${sendToBotData.sendPerson}</div>
       <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
     </div>

     <div class="msg-text">
       ${sendToBotData.talkwords} 
     </div>
   </div>`;

        publicWord.appendChild(div);
        publicWord.scrollTop = publicWord.scrollHeight;

    });

    socket.on('bot-joke', function (joke) {
        console.log(`data sent from database: ${joke}`)

        setTimeout(function () {
            //display other user data with this css
            const div = document.createElement('div');
            div.classList.add('msg');
            div.classList.add('left-msg');
            div.innerHTML = `<div
            class="msg-img"
            style="background-image: url(asset/icons8-bot.png)"
        ></div>
    
        <div class="msg-bubble">
            <div class="msg-info">
            <div class="msg-info-name">TeckyBot</div>
            <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
            </div>
    
            <div class="msg-text">
            ${joke} 😄
            </div>
        </div>`;
            publicWord.appendChild(div);
            publicWord.scrollTop = publicWord.scrollHeight;
        }, delaytTime);
    });

    socket.on('bot-luck', function (luck) {
        console.log(`data sent from database: ${luck}`)

        setTimeout(function () {
            //display other user data with this css
            const div = document.createElement('div');
            div.classList.add('msg');
            div.classList.add('left-msg');
            div.innerHTML = `<div
            class="msg-img"
            style="background-image: url(asset/icons8-bot.png)"
        ></div>
    
        <div class="msg-bubble">
            <div class="msg-info">
            <div class="msg-info-name">TeckyBot</div>
            <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
            </div>
    
            <div class="msg-text">
            ${luck} 😄
            </div>
        </div>`;
            publicWord.appendChild(div);
            publicWord.scrollTop = publicWord.scrollHeight;
        }, delaytTime);
    });

    socket.on('BotAns', function (botAnswers) {



        setTimeout(function () {
            //display other user data with this css
            const div = document.createElement('div');
            div.classList.add('msg');
            div.classList.add('left-msg');
            div.innerHTML = `<div
            class="msg-img"
            style="background-image: url(asset/icons8-bot.png)"
        ></div>
    
        <div class="msg-bubble">
            <div class="msg-info">
            <div class="msg-info-name">TeckyBot</div>
            <div class="msg-info-time">${new Date().toLocaleTimeString()}</div>
            </div>
    
            <div class="msg-text">
            ${botAnswers} 😄
            </div>
        </div>`;
            publicWord.appendChild(div);
            publicWord.scrollTop = publicWord.scrollHeight;

        }, delaytTime);
    })
};


//Bot mode logic    

document.getElementById("dark-mode").addEventListener('click', function () {
    document.documentElement.classList.toggle('dark-mode');

    document.querySelectorAll('.inverted').forEach((result) => {
        result.classList.toggle('invert');
    })
})







/*

    /////////////////////////////////////////////////////
    document
        .querySelector('#talkwords')
        .addEventListener('keyup', function (event) {
            event.preventDefault();
            //cannot alternative to avoid '\n' - event.preventDefault();
            if (event.keyCode == 13) {
                var sentence = document.querySelector('#talkwords').value;

                console.log(`sentence is -${sentence}-`);

                if (sentence == '') {
                    return;
                }

                console.log(sentence);
                addToChatBox(sentence, 'Alex');
                this.value = '';
                addToChatBox(botAnswers(sentence), 'bot');
            }
        });




/*
function addToChatBox(content, who) {
const msg = document.createElement('div');
const bubble = document.createElement('span');
const whosay = document.createElement('img');
//const time = document.createElement('div');
bubble.innerHTML = content;

bubble.classList.add('bubble');
msg.classList.add('msg');
if (who == 'bot') {
bubble.classList.add('btalk-private');
msg.classList.add('btalk-private');
whosay.classList.add('.btalk-private');
} else {
bubble.classList.add('atalk-private');
msg.classList.add('atalk-private');
whosay.classList.add('atalk-private');
}

msg.append(bubble);
const privatechatbox = document.querySelector('#private-word');
privatechatbox.appendChild(msg);
privatechatbox.scrollTop = privatechatbox.scrollHeight;
}

function botAnswers(sentences) {
const words = window.nlp(sentences);
// const words = sentence;
if (words.match('hello').found || words.match('hi').found) {
return 'Hello! How do I call you Sir/Madam?';
} else if (words.people().data().length > 0) {
const firstPersonName = words.people(0).text();
return `Hi ${firstPersonName}! Nice to meet you!`;
} else if (words.match('age').found || words.match('How old').found) {
return 'I am just several days old.';
} else {
return `Do you mean ${sentences}? Am I understanding it correctly?`;
}
}


})

*/


