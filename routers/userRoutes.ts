import express from "express";
import { Request, Response } from "express";
import { User } from "../models"
import { client } from "../main"
import { hashPassword, checkPassword } from "../hash";
import fetch from "node-fetch";
// import { checkPassword } from "../hash";
//



// userRoutes.post('/login', async (req: Request, res: Response) => {
//     try {
//         const { email, password } = req.body;
//         console.log(email, password);
//         if (!email || !password) {
//             return res
//                 .status(400)
//                 .json({ message: '1 Invalid username / password' });
//         }

//         const result = await client.query<User>('SELECT * FROM users WHERE email = $1', [email]
//         );
//         //result rows
//         const user = result.rows[0];
//         if (!user) {
//             res.status(400).json({ message: '2 Invalid username / password' });
//             return;
//         }
//         if (user.password !== password) {
//             res.status(400).json({ message: '3 Invalid username / password' });
//             return;
//         }
//         req.session['user'] = { id: user.id };
//         return res.json({ message: 'success' });
//     } catch (err) {
//         console.error(err.message);
//         return res.status(500).json({ message: 'Internal server error' });
//     }
// });


export const userRoutes = express.Router();
userRoutes.post("/login", login);
userRoutes.post("/createuser", createUser);
userRoutes.get("/login/google", loginGoogle);
userRoutes.get("/login/:question_id", selectQuizQuestion);
userRoutes.get("/login/joke", selectJokeQuestion);

async function login(req: Request, res: Response) {
    try {
        const { email, password } = req.body;
        // validation for missing value
        const user = (
            await client.query<User>(
                /*SQL*/ `
			SELECT * FROM users WHERE email = $1
		`,
                [email]
            )
        ).rows[0];
        if (!user) {
            res.status(400).json({ message: "invalid email/password" });
            return;
        }
        // console.log(password, user.password);
        const match = await checkPassword(password, user.password);
        if (!match) {
            res.status(400).json({ message: "2invalid email/password" });
            return;
        }
        req.session["user"] = { id: user.id };
        res.json({ message: "success'" });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: "internal server error" });
    }
}

async function loginGoogle(req: Request, res: Response) {
    try {

        const accessToken = req.session?.["grant"].response.access_token;
        const fetchRes = await fetch(
            "https://www.googleapis.com/oauth2/v2/userinfo",
            {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            }
        );
        const result = await fetchRes.json();
        const users = (
            await client.query(
                `SELECT * FROM users WHERE name = $1`,
                [result.email]
            )
        ).rows;

        let user = users[0];
        if (!user) {
            // generate random password
            const hashedPassword = await hashPassword("randomPassword");
            const resultInsertUser = await client.query(
                `INSERT INTO users (name,email,password) VALUES ($1, $2, $3) RETURNING id`,
                [result.email, result.email, hashedPassword]
            );
            user = resultInsertUser.rows[0];
        }
        req.session["user"] = { id: user.id };
        return res.redirect("/index.html");
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: "internal server error" });
    }
}

async function createUser(req: Request, res: Response) {
    try {
        const { username, email, password, confirmpassword } = req.body;
        // validation for missing value
        // validation for duplicated user
        const resultUserCount = await client.query(
            /*SQL*/ `
			SELECT COUNT(*) FROM users WHERE email = $1
		`,
            [email]
        );
        if (resultUserCount.rows[0]["count"] != 0) {
            res.status(400).json({ message: "duplicated user" });
            return;
        }
        if (password === confirmpassword) {
            const hashedPassword = await hashPassword(password);
            await client.query(
            /*SQL*/ `
				INSERT INTO users (name, email, password) VALUES ($1, $2, $3)
			`,
                [username, email, hashedPassword]
            );
            res.json({ message: "success'" });
        } else {
            res.status(400).json({ message: "Verification Failed! " });
            return;
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: "internal server error" });
    }
}

async function selectQuizQuestion(req: Request, res: Response) {
    //const id = req.body
    //SQL: select question_answer rom Psychological_quiz where id=<id>
    //return question_answer
    try {
        // const  {question_answer} = req.body;
        const id = parseInt(req.params.question_id);

        //const selectNumberOneToNine =id[Math.ceil(Math.random()*9)]
        const quizQuestionSelect = await client.query(
            `select * from Psychological_quiz where id = $1`,
            [id]
        )
        console.log(quizQuestionSelect.rows[0]);

        if (quizQuestionSelect.rows.length > 0) {
            return res.json(quizQuestionSelect.rows[0]);
        } else {
            return res.json({ message: "no question" })
        }

    }
    catch (err) {
        console.error(err.message);
        return res.status(500).json({ message: "internal server error_1" });
    }

}

async function selectJokeQuestion(req: Request, res: Response) {
    //const id = req.body
    //SQL: select question_answer rom Psychological_quiz where id=<id>
    //return question_answer
    try {
        // const  {question_answer} = req.body;
        const id = parseInt(req.body.question1_id);

        //const selectNumberOneToNine =id[Math.ceil(Math.random()*9)]
        const jokeQuestionSelect = await client.query(
            `select * from Joke where id = $1`,
            [id]
        )
        // console.log(jokeQuestionSelect.rows[0]);
        if (jokeQuestionSelect.rows.length > 0) {
            return res.json(jokeQuestionSelect.rows[0]);
        } else {
            return res.json({ message: "no question" })
        }

    }
    catch (err) {
        console.error(err.message);
        return res.status(500).json({ message: "internal server error" });
    }

}