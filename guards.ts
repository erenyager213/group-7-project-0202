import express from 'express';

export const isLoggedInHTML = (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) => {
    if (req.session  && req.session['user']) {
        //called Next here
        next();
    } else {
        // redirect to index page
        res.redirect('login.html');
    }
};

// export const isLoggedInAPI = (
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
// ) => {
//     if (req.session && req.session['username']) {
//         //called Next here
//         next();
//     } else {
//         // redirect to index page
//         res.status(401).json({ message: 'Unauthorized' });
//         return;
//     }
// };
