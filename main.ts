import express from 'express';
import expressSession from 'express-session';
import http from 'http';
import { Server as SocketIO, Socket } from 'socket.io';
import pg from "pg";
import dotenv from "dotenv";
import path from "path";
import { isLoggedInHTML } from "./guards";
import grant from 'grant';
import { userRoutes } from "./routers/userRoutes";
import nlp from "compromise";
import fetch from 'node-fetch';

dotenv.config();

export const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
});
client.connect();

const grantExpress = grant.express({
    "defaults": {
        "origin": "http://localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile", "email"],
        "callback": "/login/google"
    }
});

//App setup
const app = express();
const server = new http.Server(app);

//Socket setup
const io = new SocketIO(server);

const sessionMiddleware = expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false },
});

app.use(sessionMiddleware);
io.use((socket, next) => {
    const request = socket.request as express.Request;
    sessionMiddleware(
        request,
        request.res as express.Response,
        next as express.NextFunction
    );
});

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(grantExpress as express.RequestHandler);




// const API_VERSION = "/api/v1";
// app.use(API_VERSION, userRoutes);
app.use(userRoutes);

//Static file
app.use(express.static(path.join(__dirname, "public")))
app.use((req, res, next) => {
    //console.log("testing");
    next()
})

app.use(isLoggedInHTML, express.static(path.join(__dirname, "private")))

io.on('connection', onConnected);



//add a empty Set to count the online clients
let socketsConected = new Set();
let userName: string;
let arrayUserName: string[] = [];
let joke: string;
let luck: string;
let botAnswer: string;
let senderName: string;
let BotSenderName: string;
let typingName: string;
let weatherAPI: any;

async function onConnected(socket: Socket) {
    console.log('made socket connection', socket.id);


    //@ts-ignore
    if (socket.request.session["user"]) {
        const result = await client.query(
            /*SQL*/`SELECT * FROM users WHERE users.id = $1;`,
            //@ts-ignore
            [socket.request.session["user"].id]
        )

        if (result.rows.length > 0) {
            userName = result.rows[0].name

            //testing for checking if server can get the user name
            console.log(userName);
        } else {
            console.log('no name');
        }
    }
    arrayUserName.push(userName);



    socket.on('feedback', async function (data) {

        //@ts-ignore
        if (socket.request.session["user"]) {
            const result = await client.query(
                /*SQL*/`SELECT * FROM users WHERE users.id = $1;`,
                //@ts-ignore
                [socket.request.session["user"].id]
            )

            if (result.rows.length > 0) {
                typingName = result.rows[0].name

                //testing for checking if server can get the user name
                console.log(typingName);
            } else {
                console.log('no name');
            }
        };

        data.typingPerson = typingName;

        socket.broadcast.emit('feedback', data);
    })




    //emit the user name array list to socket
    io.emit('clients-list', arrayUserName);

    //add user id of every connection into the Set 
    socketsConected.add(socket.id);

    console.log(socketsConected);
    // emit the number of Set to socket
    io.emit('clients-total', socketsConected.size)

    //listen to disconnect and delelte the user id in Set
    socket.on('disconnect', function () {
        console.log(`[INFO] ${socket.id} is Disconnected`);
        socketsConected.delete(socket.id);
        io.emit('clients-total', socketsConected.size)

        let indexOfUsername = arrayUserName.indexOf(userName);
        arrayUserName.splice(indexOfUsername, 1);
        io.emit('clients-list', arrayUserName);
    });

    //listen to the data sent from type 'chat'
    socket.on('chat', async function (data) {
        //@ts-ignore
        if (socket.request.session["user"]) {
            const result = await client.query(
                /*SQL*/`SELECT * FROM users WHERE users.id = $1;`,
                //@ts-ignore
                [socket.request.session["user"].id]
            )

            if (result.rows.length > 0) {
                senderName = result.rows[0].name

                //testing for checking if server can get the user name
                console.log(senderName);
            } else {
                console.log('no name');
            }
        };

        data.sendPerson = senderName;
        console.log(data.talkwords + data.sendPerson + " " + data.time);
        console.log(data)

        // broadcast the data to every clients except the sender himself
        socket.broadcast.emit('chat', data);
    })


    socket.on('bot-chat', async function (sendToBotData) {

        //@ts-ignore
        if (socket.request.session["user"]) {
            const result = await client.query(
                /*SQL*/`SELECT * FROM users WHERE users.id = $1;`,
                //@ts-ignore
                [socket.request.session["user"].id]
            )

            if (result.rows.length > 0) {
                BotSenderName = result.rows[0].name

                //testing for checking if server can get the user name
                console.log(BotSenderName);
            } else {
                console.log('no name');
            }
        };

        sendToBotData.sendPerson = BotSenderName;

        socket.broadcast.emit('bot-chat', sendToBotData);

        const words = sendToBotData.talkwords;
        const nlpWords = nlp(sendToBotData.talkwords);

        if (nlpWords.match('joke').found || words.includes("笑話")) {
            const id = Math.floor(Math.random() * 8 + 1);
            const jokeQuestionSelect = await client.query(
            /*SQL*/`select * from Joke where id = $1`,
                [id]
            )
            // console.log(jokeQuestionSelect.rows[0]);
            if (jokeQuestionSelect.rows.length > 0) {

                joke = jokeQuestionSelect.rows[0].question_answer

                console.log(jokeQuestionSelect.rows[0]);

                //emit the joke to socket
                io.emit('bot-joke', joke);

            } else {
                console.log('no joke');
            }
        } else if (nlpWords.match('luck').found || words.includes("運程")) {
            const id = Math.floor(Math.random() * 8 + 1);
            const psychologicalQuizSelect = await client.query(
            /*SQL*/`select * from psychological_quiz where id = $1`,
                [id]
            )
            if (psychologicalQuizSelect.rows.length > 0) {

                luck = psychologicalQuizSelect.rows[0].question_answer

                console.log(psychologicalQuizSelect.rows[0]);

                //emit the joke to socket
                io.emit('bot-luck', luck);

            } else {
                console.log('no luck');

            }

        } else if (nlpWords.match('hello').found || nlpWords.match('hi').found) {
            botAnswer = 'Hello! How do I call you Sir/Madam?';
            io.emit('BotAns', botAnswer);
            //@ts-ignore
        } else if (nlpWords.people().data().length > 0) {
            const firstPersonName = nlpWords.people(0).text();
            botAnswer = `Hi ${firstPersonName}! Nice to meet you!`;
            io.emit('BotAns', botAnswer);
        } else if (nlpWords.match('age').found || nlpWords.match('How old').found) {
            botAnswer = 'I am just several days old.';
            io.emit('BotAns', botAnswer);
        } else if (nlpWords.match('rain').found || nlpWords.match('sun').found) {
            botAnswer = '天氣不好';
            io.emit('BotAns', botAnswer);
        } else if (words.includes("早晨")) {
            botAnswer = "早晨";
            io.emit("BotAns", botAnswer);
        }else if (words == "你好") {
            botAnswer = "你好呀";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("你好嗎")) {
            botAnswer = "我覺得幾好呀";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("是啊")) {
            botAnswer = "我能幫你什麼嗎";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("哈哈")) {
            botAnswer = "你笑得好假wor";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("謝謝") || words.includes("唔該")) {
            botAnswer = "唔洗客氣";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("新聞")) {
            botAnswer = "什麼新聞";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("顏色")) {
            botAnswer = "咁你鍾意最什麼顏色";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("電影")) {
            botAnswer = "咁你鍾意邊款電影，大家一齊睇";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("愛") || words.includes("鍾意")) {
            botAnswer = "收到";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("好啊")) {
            botAnswer = "我都係咁諗喎咁啱嘅";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("咁啱嘅")) {
            botAnswer = "我哋真係心有靈犀";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("早抖")) {
            botAnswer = "早抖啦";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("拜拜")) {
            botAnswer = "拜拜";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("再見")) {
            botAnswer = "再見";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("唔好意思") || words.includes("有心") || words.includes("幾好嗎")
            || words.includes("錢") || words.includes("惠") || words.includes("金") || words.includes("先生")
            || words.includes("小姐") || words.includes("好耐冇見") || words.includes("唔使") || words.includes("哇")) {
            botAnswer = "講呢啲";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("電話")) {
            botAnswer = "唔好意思我依家唔可以打電話";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("幾點")) {
            botAnswer = "睇返你手機啦";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("到時見")) {
            botAnswer = "到時見";
            io.emit("BotAns", botAnswer);

        } else if (words.includes("天氣")) {
            let response = await fetch('https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=rhrread&lang=tc');
            weatherAPI = await response.json();
            const temp =  weatherAPI.temperature.data.filter((place:any)=>place.place === "青衣");
            console.log(JSON.stringify(temp));
            io.emit("BotAns","依家香港溫度大約係" +temp[0].value+ "°C");

        } else if (words.includes("靚")) {
            botAnswer = "你咪咁鬼客氣啦";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("旅行")) {
            botAnswer = "我都想去旅行";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("音樂")) {
            botAnswer = "我都想聽音樂";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("股票")) {
            botAnswer = "股票梗係有賺有蝕㗎啦";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("明星")) {
            botAnswer = "邊個明星";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("運動")) {
            botAnswer = "平時做開什麼運動啊";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("節目")) {
            botAnswer = "有咩節目";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("正")) {
            botAnswer = "你仲正";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("爽")) {
            botAnswer = "大家一齊爽";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("老闆") || words.includes("老細") || words.includes("大佬")) {
            botAnswer = "有咩貴幹";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("隨便")) {
            botAnswer = "唔使客氣";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("香港")) {
            botAnswer = "香港係一個好地方";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("官人")) {
            botAnswer = "你想點";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("變態")) {
            botAnswer = "邊有呀";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("仆") || words.includes("老母") || words.includes("屌") || words.includes("冚")
            || words.includes("戇") || words.includes("肺") || words.includes("大檸樂") ||
            words.includes("收皮") || words.includes("打飛機") || words.includes("柒") || words.includes("鳩")
        ) {
            botAnswer = "你咁講唔啱㗎";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("東區") || words.includes("中西區;") || words.includes("南區")
            || words.includes("灣仔") || words.includes("九龍") || words.includes("九龍城")
            || words.includes("觀塘") || words.includes("深水埗區") || words.includes("黃大仙")
            || words.includes("油尖旺") || words.includes("新界") || words.includes("離島") || words.includes("葵青") || words.includes("北區") || words.includes("西貢") || words.includes("沙田") || words.includes("大埔") || words.includes("荃灣") || words.includes("屯門") || words.includes("元朗") || words.includes("馬鞍山") || words.includes("屯門")) {
            botAnswer = "呢個係香港一個好地方";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("飲酒")) {
            botAnswer = "一齊飲";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("倫敦") || words.includes("紐約") || words.includes("新加坡")
            || words.includes("上海") || words.includes("北京") || words.includes("杜拜")
            || words.includes("巴黎") || words.includes("東京") || words.includes("首爾")
            || words.includes("柏林") || words.includes("美國") || words.includes("韓國")
            || words.includes("台灣") || words.includes("中國") || words.includes("英國")
            || words.includes("日本") || words.includes("澳洲") || words.includes("紐西蘭")
            || words.includes("馬來西亞") || words.includes("亞洲") || words.includes("非洲")
            || words.includes("美洲") || words.includes("歐洲") || words.includes("大洋洲")
            || words.includes("南美洲") || words.includes("大阪") || words.includes("九州")
            || words.includes("台北") || words.includes("高雄") || words.includes("長洲")
            || words.includes("南丫島") || words.includes("俄羅斯") || words.includes("大陸")
            || words.includes("深圳") || words.includes("加州") || words.includes("太平洋")
            || words.includes("火星") || words.includes("金星") || words.includes("地球")
            || words.includes("宇宙") || words.includes("木星") || words.includes("宇宙")) {
            botAnswer = "呢個係一個好地方";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("咁就好") || words.includes("係")) {
            botAnswer = "好啊";
            io.emit("BotAns", botAnswer);
        }
        else if (words.includes("time")) {
            botAnswer = "Its still early";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("year")) {
            botAnswer = "It is 2021 now";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("people")) {
            botAnswer = "people are afraid of what they do not understand";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("way")) {
            botAnswer = "the way you smile make me a day";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("day")) {
            botAnswer = "day by day";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("man")) {
            botAnswer = "Come on man";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("thing")) {
            botAnswer = "What do you want";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("woman")) {
            botAnswer = "The plural women is sometimes used for female humans regardless of age";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("life") || words.includes("lives")) {
            botAnswer = "You only lived onces";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("enjoy")) {
            botAnswer = "Enjoy tonight";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("child")) {
            botAnswer = "take care of you child";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("world")) {
            botAnswer = "I love the world";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("school")) {
            botAnswer = "I go to school by bus";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("state")) {
            botAnswer = "What do you mean?";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("family")) {
            botAnswer = "Family is not an important thing. It is everything";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("student")) {
            botAnswer = "knowledge is power";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("group")) {
            botAnswer = "Join us ";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("country")) {
            botAnswer = "Which country do you want to travel";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("problem")) {
            botAnswer = "sorry, I cant help you";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("hand")) {
            botAnswer = "Hands have their own language.";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("part")) {
            botAnswer = "every part is necessary";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("place")) {
            botAnswer = "Do I hold a place in your heart?";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("case")) {
            botAnswer = "In which case?";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("week")) {
            botAnswer = "Every week is essential";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("company")) {
            botAnswer = "I may help you find a job";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("system")) {
            botAnswer = "The power beyond the system is existed";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("program")) {
            botAnswer = "I love program";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("question")) {
            botAnswer = "To be, or not to be: what a question!";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("work")) {
            botAnswer = "work hard,play hard";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("money")) {
            botAnswer = "All the money you made will never buy back you soul";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("friend")) {
            botAnswer = "Friendship is the hardest thing in the world to explain";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("morning")) {
            botAnswer = "Good morning";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("evening")) {
            botAnswer = "Good evening";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("night")) {
            botAnswer = "Good night";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("person")) {
            botAnswer = "You are a nice person";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("war")) {
            botAnswer = "This is a war to end all wars";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("party")) {
            botAnswer = "Let go party tonight";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("back")) {
            botAnswer = "I cant go back anymore";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("name")) {
            botAnswer = "What is your name?";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("end")) {
            botAnswer = "It never ned";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("game")) {
            botAnswer = "No game no life";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("lot")) {
            botAnswer = "That is really really a lot";
            io.emit("BotAns", botAnswer);
        } else if (words.includes("water")) {
            botAnswer = " Do you want some water?";
            io.emit("BotAns", botAnswer);
        }
        else {
            botAnswer = `唔好意思，你係咪指 ${words}?　我唔係好明你講咩！ `;
            io.emit('BotAns', botAnswer);
        };
    });
};

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`connected to localhost:${PORT}`);
});
