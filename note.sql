
1.CREATE DATABASE chatboxdb;

2. CREATE TABLE Users(
    id SERIAL primary key,
    name VARCHAR(255) not null,
    email VARCHAR(255) not null,
    password VARCHAR(255) not null
);

3.CREATE TABLE Ranking(
    id SERIAL primary key,
    level INTEGER not null,
    user_id INTEGER not null,
    question_id INTEGER not null,
    iscorrect BOOLEAN
);

4.CREATE TABLE Psychological_quiz(
    id SERIAL primary key,
    number_question INTEGER not null,
    question_answer VARCHAR(255) not null
);


6.CREATE TABLE Joke(
    id SERIAL primary key,
    number_question INTEGER not null,
    question_answer VARCHAR(255) not null
);



8.CREATE TABLE Mutiplayer_competition(
    id SERIAL primary key,
    question_id INTEGER not null,
    set_id INTEGER not null
);


9.CREATE TABLE Five_question(
    id SERIAL primary key,
    question VARCHAR(255) not null,
    question_answer VARCHAR(255) not null
);

10. insert into Users (name, email, password) VALUES ('dummy1', 'dummy@123.com','1234');
    select * from Users;

11. 
insert into psychological_quiz(number_question, question_answer)VALUES('1', '如果你單身並尋找愛情，你很可能會遇到一個新對象。你需要多結識新朋友。 如果你處於長期關係或已婚，那麼這張卡片就預示著堅定承諾。你的丈夫或靈魂伴侶充滿激情地愛你，所有的障礙和爭吵都會消失。未來充滿了最大的幸福。');

12. 
insert into psychological_quiz(number_question, question_answer)VALUES('2', '你與某人的聯繫可能是如此強烈，所以其他的衝動似乎不那麼重要。 
如果你是一個單身男人，這張卡可以讓你遇見理想情人，而如果你和女人約會，它可能會讓你對一個遙遠而無法接近的人產生不懈的興趣。 
對於單身女性，這張卡片承諾您將成為許多人關注的焦點。 但是，你需要明智地用自己的直覺來分析你目前對象究竟對你是一種祝福，或者你是無用地追逐而無法實現的。
其他條件下的解釋');

13. 
insert into psychological_quiz(number_question, question_answer)VALUES('3', '如果你是單身並尋找愛情，很快就會遇到一個認真並尋求忠誠的關係的人。 那些已經處於關係中的人會發現他們的關係日新月異，越來越深。 即使您遇到問題，卡片也會建議您等待情況穩定下來。 
女人可能會發現自己被年長的男人所吸引，從一開始可能沒有相互的感情，但只要有耐心，這種關係就會綻放，因為你的伴侶是一個按順序和邏輯生活的成熟男人。另一方面，男人需要更積極，以便找到合適的靈魂伴侶。 也許你在表達自己的感受時猶豫不決，不確定你是否被接受。 但抓住時機，向你愛的女人告白。
');

14. 
insert into psychological_quiz(number_question, question_answer)VALUES('4', '如果你選這，這表明你的恐懼，成癮和其他負面衝動會限制你並讓你把它們視為無法控制的事物。因此，您無法前進或採取任何建設性步驟。 你也可能感到絕望或痴迷，甚至對生活抱有悲觀的看法。 該卡建議您擺脫這些負面模式，為您的生活帶來積極的變化。如果你是單身並尋找愛情，那麼此牌反映了你的絕望，最有可能驅走那些對你感興趣的人。多花時間去了解自己。此後，當你更有成就感時，繼續你的任務。 那些處於長期關係中的人可能會發現這種關係是一種負擔 – 一種被困的感覺。該卡會啟動您退後一步，找出可以保存彼此關係的方式
其他條件下的解釋');

15. 
insert into psychological_quiz(number_question, question_answer)VALUES('5', '你吸引人的舉止鼓勵每個人更接近你。為了獲得你的感情而焦慮，他們隨時準備為你做任何事情。 
如果你是單身並尋找愛情，那麼放鬆一下，因為愛會來到你的路上，你會得到一個與你的精神在各方面  `都相匹配的伴侶。它甚至可能涉及你即將到來的婚姻。 
那些處於戀愛關係中的人會發現彼此的關係越來越強大。然而，隨著所有事情的發生，它可能會在情感上被帶走。因此，花時間決定什麼是最好的，然後採取最適合任何特定情況的行動方案。
其他條件下的解釋
');


16. 
insert into psychological_quiz(number_question, question_answer)VALUES('6', '
職業和工作： 只有當你按照規則組織和工作時，才會保證工作的進展。
商業和金融： 該卡建議您了解從銀行家或其他有經驗的人處理資金的方式，然後進行穩定的投資。它還強調保持在你的預算範圍內，以便沒有緊張的生活。
友誼：  保持與朋友的承諾，慶祝小小的快樂並享受長久的聯繫。');

17. 
insert into psychological_quiz(number_question, question_answer)VALUES('7', '如果你單身並尋求愛情，你的等待很快就會結束。當然，只有當你開始與新人交往並表達你的真實感受時，才會發生這種情況。這可能是展示您的自信和魅力的最佳時機。 
那些在分手後一直處於低潮的人會很快反彈並找到真正的愛情。
其他條件下的解釋 健康：該卡表示健康狀況良好或正在改善。你需要通過均衡的飲食和定期運動來改變你現在的生活方式。');

18. 
insert into psychological_quiz(number_question, question_answer)VALUES('8', '這張卡可能會為你帶來來自異性的急需的愛和關注。 與此同時你應該更真誠，這對於改善你未來的關係是有用的。 如果你是單身，不要驚訝看到你更好的一半來敲你的門。 表現出一些渴望，以使您的潛在合作夥伴感到安全。 很快地在關係中可能提升到一個新的水平，可能是婚姻。 只要雙方都滿足，彼此關係就會日益強大。
其他條件下的解釋 ');

19. 
insert into psychological_quiz(number_question, question_answer)VALUES('9', '如果你是一個單身男人，這張卡可以讓你遇見理想情人，而如果你和女人約會，它可能會讓你對一個遙遠而無法接近的人產生不懈的興趣。 
對於單身女性，這張卡片承諾您將成為許多人關注的焦點。 但是，你需要明智地用自己的直覺來分析你目前對象究竟對你是一種祝福，或者你是無用地追逐而無法實現的。職業和工作： 只有當你按照規則組織和工作時，才會保證工作的進展。該卡表示健康狀況良好或正在改善。你需要通過均衡的飲食和定期運動來改變你現在的生活方式。');


20. 
insert into Joke(number_question, question_answer)VALUES('1', '杏壇女子中學舉行春季旅行，一行百多人來到農場參觀。
第一站農場主人教大家擠牛奶，示範完畢，到大家動手親自試試。半小時後，一個女生看到別人已經擠了小半筒，而自己的只有一點點，非常不解。於是，農場主人過來，看了看說：「小姐，你不但擠錯了地方，而且還選錯了牛。');

21.
insert into Joke(number_question, question_answer)VALUES('2', '皇后似咩？似廣場，因為皇后像廣場');

22.
insert into Joke(number_question, question_answer)VALUES('3', '醫院裡病人問：醫生，我的病這樣嚴重，你看還有希望嗎？
醫生充滿信心的說：我敢保證，你這種病十人九亡，我治療了九個像這樣的病人都死了，你是第十個，絕對可以康復');

23.
insert into Joke(number_question, question_answer)VALUES('4', '話說有一天瑪麗蓮夢露死後要往天堂之路，可是這中間要經過一座橋。瑪麗蓮夢露走 到半路時耶穌出現了，然後跟瑪麗蓮夢露說：如果妳心無邪念你會一路走完然後直往 天堂，但是如果妳心有邪念妳就會掉下橋去而落到地獄去.然後說完之後走了幾步突然就掉下橋去了。');

24.
insert into Joke(number_question, question_answer)VALUES('5', '某女深夜騎單車經過辛亥隧道,中途單車"落鏈" 只好在路旁招手求助只見所有慢車道的車看見她, 都快速閃到快車道去了..... 正沮喪時,一輛小轎車掠過眼前, 居然又回來..... 她心中一陣欣喜,欲迎向前....卻見小轎車後車窗搖下,迎面灑出一疊紙錢........');

25.
insert into Joke(number_question, question_answer)VALUES('6', '小明在路上撿到了一百元，走進了藥房，對老闆大聲說：老闆，我要買好自在！老闆當然是驚訝不已:小孩子買好自在做什麼？小孩一臉天真的回答：因為廣告都說用了好自在"能騎馬，游泳，騎腳踏車啊！');

26.
insert into Joke(number_question, question_answer)VALUES('7', '有氣質國文老師出了個對聯要考考他的學生，
上聯是『大魚吃小魚，小魚吃蝦，蝦吃水，水落石出他要學生明天把下聯對好。一位學生當晚百思不得其解，於是到外面閒逛尋找靈感，走著走著便走到老師家，突然聽到房內傳來異聲，便好奇的從窗戶偷看，不看也罷，一看之下不得了，靈感如滔滔江水湧出，於是乎第二天他便交出他的下聯如下：先生壓師娘，師娘壓床，床壓地，地動山搖');

27. 
insert into Joke(number_question, question_answer)VALUES('8', '明仔：爸爸，點解會有兩個朗拿度？爸爸：一個係朗拿度，一個係 C. 朗拿度明仔：咦，三國時代都有兩個馬昭喎爸爸：邊度有呀？明仔：一個係馬昭 , 一個係司馬昭 ...');

28. 
insert into Joke(number_question, question_answer)VALUES('9', '華盛頓砍低左佢老豆既樹，點解佢老豆冇鬧佢，仲讚佢乖，話佢誠實？因為華盛頓果時仲拎住個斧頭');
