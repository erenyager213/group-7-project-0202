// window.onload = () => {
//     initLoginForm();
// };

// function initLoginForm() {
//     const form = document.getElementById('login-form');
//     form.addEventListener('submit', async function (event) {
//         event.preventDefault();
//         const jsonObject = {};
//         jsonObject['email'] = form.email.value;
//         jsonObject['password'] = form.password1.value;
//         const res = await fetch('/login', {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json; charset=utf-8',
//             },
//             body: JSON.stringify(jsonObject),
//         });
//         if (res.status !== 200) {
//             const data = await res.json();
//             alert(data.message);
//         } else {
//             window.location = '/chatroom.html';
//         }
//     });
// }

window.onload = () => {
    const form = document.getElementById("login-form");
    form.addEventListener("submit", async function (event) {
        event.preventDefault();
        const jsonObject = {};
        jsonObject["email"] = form.email.value;
        jsonObject["password"] = form.password1.value;
        const res = await fetch("/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(jsonObject),
        });
        if (res.status !== 200) {
            const data = await res.json();
            alert(data.message);
        } else {
            window.location = "/index.html";
        }
    });
};
