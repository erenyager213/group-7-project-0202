window.onload = () => {
    const createuserform = document.getElementById("create-form");
    createuserform.addEventListener("submit", async function (event) {
        event.preventDefault();
        const jsonObject = {};
        jsonObject["email"] = createuserform.email.value;
        jsonObject["username"] = createuserform.newusername.value;
        jsonObject["password"] = createuserform.password1.value;
        jsonObject["confirmpassword"] = createuserform.password2.value;
        const res = await fetch("/createuser", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(jsonObject),
        });
        if (res.status !== 200) {
            const data = await res.json();
            alert(data.message);
        } else {
            alert("Success! Login and Start");
            window.location = "/index.html";
        }
    });
};